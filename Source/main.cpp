//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>


class MidiMessage
{
public:
    MidiMessage() //Constructor
    {
        number = 60;
    }
    MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel)
    {
        number = initialNoteNumber;
        velocity = initialVelocity;
        channel = initialChannel;
    }
    ~MidiMessage() //Destructor
    {
 
    }
    void setNoteNumber (int value) //Mutator
    {
        if (value >= 127) {
            number = 127;
        }
        else if (value <= 0) {
            number = 0;
        }
        else {
            number = value;
        }
    }
    int getNoteNumber() const //Accessor
    {
        return number;
    }
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    void setNoteVelocity (int value)
    {
        if (value >= 127){
            velocity = 127;
        }
        else if (value <= 0) {
            velocity = 0;
        }
        else {
            velocity = value;
        }
    }
    float getFloatVelocity() const
    {
        return velocity / 127.0;
    }
    void setNoteChannel (int value)
    {
        if (value >= 127) {
            channel = 127;
        }
        else if (value <= 0) {
            channel = 0;
        }
        else {
        channel = value;
        }
    }
    int getNoteChannel() const
    {
        return channel;
    }

private:
    int number;
    int velocity;
    int channel;
};

int main (int argc, const char* argv[])
{
    
    // insert code here...
    //std::cout << "Hello, World!\n";
    MidiMessage note (45, 93, 2);
    

    //note.setNoteNumber(129);
    //note.setNoteVelocity(-4);
    std::cout << "Note Number = " << note.getNoteNumber() << std::endl;
    std::cout << "Frequency = " << note.getMidiNoteInHertz() << std::endl;
    std::cout << "Amplitude = " << note.getFloatVelocity() << std::endl;
    std::cout << "Channel = " << note.getNoteChannel() << std::endl;
    return 0;
}
